# flowchart4926

[ГАРАНТ: Указание Банка России от 8 октября 2018 г. № 4926-У](https://www.garant.ru/products/ipo/prime/doc/72039474/)
![Указание Банка России от 8 октября 2018 г. № 4926-У](https://github.com/vicnjan/MyFlowChart/blob/master/4926-%D0%A3.svg)

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```